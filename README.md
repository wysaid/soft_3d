<h1> SIMPLE SOFT 3D </h1>

<p>本项目主要使用C++完成了OpenGL大部分矩阵和向量操作，为这方面的计算提供便利。同时附带两个小demo以供展示。矩阵向量算法部分代码应该可以兼容各个支持C++的编译器（iOS下可以mm文件后缀方式混用以及安卓下使用NDK编译）。</p>

<p>本版本由wysaid依据其原理编写而成，部分代码来自于OpenGL.org，文件中有标注</p>
<p>本版本已内置EGE13.04版，无需安装EGE。欢迎各位感兴趣的同学取用参考</p>
<p>LICENSE 请参照The MIT License</p>
<h2>ScreenShot1</h2>
<p><img src="https://raw.github.com/wysaid/SOFT_3D/master/screenshot0.jpg"></p>

<h2>ScreenShot2</h2>
<p><img src="https://raw.github.com/wysaid/SOFT_3D/master/screenshot1.jpg"></p>

<h2>ScreenShot3</h2>
<p><img src="https://raw.github.com/wysaid/SOFT_3D/master/screenshot2.jpg"></p>